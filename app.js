$(document).ready(function() {
    
    $(document.body).on('click', '.tabs-navi button', function() {
        tabChanger($(this));
    });
    
    $(document.body).on('click', '.show-details', function() {
        $(this).parent('div').next('ul').slideToggle();
    });

    $(document.body).on('click', '.scrolled-col input', function() {
        let _level = $(this).data('level');
        if (_level === 1) {
            $(this).parents('.grouped').next('ul').find('input').prop('checked', false);
        }
        else if (_level === 2) {
            $(this).parents('.level-1').children('li').children('.grouped').find('input').prop('checked', false);
            $(this).parents('.level-1').children('li').children('.grouped').find('input').css('checked', false);
            $(this).parents('.grouped').next('ul').find('input').prop('checked', false);
        }
        else if (_level === 3) {
            $(this).parents('.level-1').children('li').children('.grouped').find('input').prop('checked', false);
            $(this).parents('.level-2').children('.grouped').find('input').prop('checked', false);
        }
    });

});

const tabChanger = (el) => {
    $(`.tabs-content .scrolled-col`).hide();
    let _index = el.parent('li').index() + 1;
    $(`.tabs-content .scrolled-col:nth-child(${_index})`).show();
}